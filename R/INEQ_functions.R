#' Download the INEQ press coverage table
#'
#' Generates a LaTeX table from the INEQ press coverage table on the homepage
#' @param years (Vector of) years to download for
#' @param outfile An optional .tex file to save the output to
#' @export
getPressespiegel <- function(years=c(2000:2099), outfile="") {
  # Downloads the Pressespiegel from wu.ac.at/ineq
  # and generate a nice LaTeX table
  require(RCurl, quietly=TRUE)
  require(XML, quietly=TRUE)
  require(stringr, quietly=TRUE)
  require(Hmisc, quietly=TRUE)
  presse.url <- "https://www.wu.ac.at/ineq/presse/pressespiegel/"
  presse <- htmlParse(getURL(presse.url, .encoding="UTF-8"), encoding="UTF-8")
  time <- unlist(lapply(getNodeSet(presse, "//h2/text()"), xmlValue))
  time <- str_extract(time, "Q[0-9]/([0-9]{4})")
  tab <- getNodeSet(presse, "//div[@class='acc-item-body']//table")
  tab <- lapply(tab, readHTMLTable, stringsAsFactors=FALSE)
  yrs <- str_extract(time, "[0-9]{4}")
  tab <- tab[yrs%in%years]
  time <- time[yrs%in%years]
  tab.full <- do.call("rbind", tab)
  tab.full$V1 <- gsub('\\"|PDF', "", tab.full$V1)
  cmds <- matrix("", nrow=nrow(tab.full), ncol=ncol(tab.full[,1:2]))
  cmds[,1] <- "itshape"
  latex(tab.full[,1:2], title="Pressespiegel", colheads=c("Artikel", "Person"), rowname="", multicol=FALSE,
        rgroup=time, n.rgroup=unlist(lapply(tab, nrow)), booktabs=TRUE, file=outfile, col.just=c("p{7cm}", "p{6cm}"), size="small",
        cellTexCmds=cmds, longtable=TRUE)
}
